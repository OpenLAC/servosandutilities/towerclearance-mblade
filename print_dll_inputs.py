# -*- coding: utf-8 -*-
"""
Write to file the lines for the tower input DLL for a list of htc files

The user defines a collection of relative blade stations where the 
tower clearance should be checked. All blades have the same defined
stations for simplicity.

Author: Jennifer Rinker, rink@dtu.dk
"""
import numpy as np
import os

# script inputs
htc_paths    = ['file1_path.htc','file2_path.htc']      # paths to htc files
bld_stns     = np.arange(0.8,1.01,0.025)                # rel blade stns to check clearance [%]
num_blds     = 3                                        # number of blades
out_path     = os.path.join('.',
                  'tower_clearance_DLL_text.txt')       # path to output text file with dll text
dll_name     = 'towerclearance_mblade'                  # name of tower clearance dll
dll_dir      = os.path.join('.','control')              # relative path to dll in htc dir
twr_rads     = [4.15, 2.75]                             # tower radius @ bottom, top [m]


# -----------------------------------------------------------------------------
# should not need to change anything below this point
                         
# define some useful intermediate variables
dll_path     = os.path.join(dll_dir,dll_name)           # rel path to DLL in htc dir
num_bld_pts  = num_blds*len(bld_stns)                   # tot num blade points to check clearance
num_dll_inps = 6 + num_bld_pts*3                        # tot num of DLL input channels
                    
# open text file for writing dll info to
with open(out_path, 'w') as out_file:
    
    # loop through htc fiiles to analyze
    for htc_path in htc_paths:
        
        # write name of htc file to output file
        out_file.write('\n{}\n\n'.format(htc_path))
        
        # initialize useful parameters
        main_body = 0       # flag - are we in main body block
        name      = ''      # name of main body
        c2_def    = 0       # flag - are we in c2_def block
        sec       = 0       # flag - are we in a "sec" line in c2_def
        nodes     = {}      # dictionary with tower and blade node data
        
        # open htc file to read from it
        with open(htc_path,'r') as htc_file:
            for line in htc_file:
                
                # toggle main body flags
                if ('begin' in line) and ('main_body' in line):
                    main_body = 1
                elif ('end' in line) and ('main_body' in line):
                    main_body = 0
                    
                # if we're in a main body
                if main_body:
                    
                    # toggle c2_def flags
                    if ('begin' in line) and ('c2_def' in line):
                        c2_def = 1
                    elif ('end' in line) and ('c2_def' in line):
                        c2_def = 0
                        sec    = 0
                    
                    # get the body name if we're on correct line
                    if ('name' in line) and ('filename' not in line):
                        name = line.split()[1]
                        
                    # if we're in a section of c2_def, get node information
                    if sec and (name in ['tower','blade1']):
                        nodes[name][sec-1] = np.abs(float(line.split()[4]))
                        sec += 1
                        
                    # get the number of sections if we're on that line
                    if c2_def and ('nsec' in line):
                        nsec  = int(line.split(';')[0].split()[1])
                        sec   = 1
                        
                        # initialize arrays for list of tower node points
                        if (name in ['tower','blade1']):
                            nodes[name] = np.empty(nsec)
        
        # write DLL preamble
        out_file.write(';	--- DLL for tower-blade tip distance -- ;\n')
        out_file.write('  begin type2_dll; \n')
        out_file.write('    name {} ; tower clearance DLL\n'.format(dll_name))
        out_file.write('    filename  {} ;\n'.format(dll_path))
        out_file.write('    dll_subroutine_init initialize ;\n')
        out_file.write('    dll_subroutine_update update ;\n')
        out_file.write('    arraysizes_init  1 3 ;\n')
        out_file.write('    arraysizes_update  {:.0f} 6 ;\n'.format(num_dll_inps))
        out_file.write('    begin init ;    Variables passed into initialization function\n')
        out_file.write('      constant  1 {:.2f}  ;'.format(twr_rads[0]) + \
                       ' Tower radius at tower bottom [m] \n')
        out_file.write('      constant  2 {:.2f}  ;'.format(twr_rads[1]) + \
                       ' Tower radius at tower top [m]\n')
        out_file.write('      constant  3   {:.0f}  ;'.format(num_bld_pts) + \
                       ' Number of blade points to check [-]\n')
        out_file.write('    end init ;\n')
        out_file.write('    begin output;   Variables passed into update function\n')

        
        # write tower node DLL inputs (base and tip, hard-coded into DLL)
        out_file.write('{:>10s}{:>6s}{:>4s}{:>7s}{:4.0f}{:6.2f}{:>7s} ; {:s}\n'.format(
                       'mbdy','state','pos','tower', 1, 0.0,'global',
                       '[1,2,3] global coordinates of tower base'))
        out_file.write('{:>10s}{:>6s}{:>4s}{:>7s}{:4.0f}{:6.2f}{:>7s} ; {:s}\n'.format(
                       'mbdy','state','pos','tower',len(nodes['tower'])-1,
                       1.0,'global','[4,5,6] global coordinates of tower base'))
        
        # write blade node DLL inputs
        name = 'blade1'     # name blade info stored under in node dictionary
        i_output = 1        # index of blade DLL line
        for i_bld in range(1,1+num_blds):
            for i_stn, rel_stn in enumerate(bld_stns):
                
                # get element number and relative distance from defined blade stations
                if abs(rel_stn - 1.) < 1e-6:
                    el_num   = len(nodes[name]) - 1
                    rel_dist = 1.
                else:
                    abs_stn = rel_stn * nodes[name][-1]                 # absolute loc
                    el_num   = (abs_stn < nodes[name]).argmax()         # element num
                    rel_dist = (abs_stn - nodes[name][el_num-1]) / \
                                (nodes[name][el_num] - nodes[name][el_num-1])
                
                # define comment message for DLL line
                i_pt    = 7 + (i_output - 1) * 3
                comment = '[{:.0f},{:.0f},{:.0f}] global'.format(i_pt,
                                                                 i_pt + 1,
                                                                 i_pt + 2) \
                            + ' coordinates of point {:.0f} '.format(i_output) \
                            + '(blade {:.0f} {:.1f}% R)'.format(i_bld,100.*rel_stn)
                            
                # write DLL line
                out_file.write('{:>10s}{:>6s}{:>4s}{:>7s}{:4.0f}{:6.2f}{:>7s} ; {:s}\n'.format(
                               'mbdy','state','pos','blade{:.0f}'.format(i_bld),
                               el_num, rel_dist,'global', comment))
                
                i_output += 1
                
        # dll ending
        out_file.write('    end output;    \n')
        out_file.write('  end type2_dll;\n')
    