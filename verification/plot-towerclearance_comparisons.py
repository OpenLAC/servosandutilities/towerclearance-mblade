# -*- coding: utf-8 -*-
"""
Create plots to compare the outputs from the tower clearance DLLs for the 
2-bladed and 3-bladed turbines.

Author: Jennifer Rinker, rink@dtu.dk
"""
import matplotlib.pyplot as plt
import numpy as np
import os
from   wetb.hawc2.Hawc2io import ReadHawc2

res_dir = 'Y:\\DTU10MW\\H0005\\res'
fig_dir = 'M:\\Documents\\miscellaneous\\writeups\\2017-04-06_tower_clearance'

save_figs = 0

# ================ FIGURE 1 ================
# DTU 10 MW geometry

# define geometries
l_tower = 115.63
l_ttop  = 2.75
l_shaft = 7.1
l_hub   = 2.8
l_bld   = 86.366
l_pbnd  = 3.332
tilt    = 5. * np.pi / 180.
cone    = 2.5 * np.pi / 180.
r_tbase = 4.15
r_ttop  = 2.25

# calculate points
x_orig  = np.array([0,0])
x_ttop  = np.array([0,l_tower])
x_shft  = x_ttop + np.array([0,l_ttop])
x_hbctr = x_shft + l_shaft*np.array([np.cos(np.pi-tilt),
                                     np.sin(np.pi-tilt)])
x_bldrt = x_hbctr + l_hub*np.array([np.cos(1.5*np.pi-tilt-cone),
                                    np.sin(1.5*np.pi-tilt-cone)])
x_bldln = x_bldrt + l_bld*np.array([np.cos(1.5*np.pi-tilt-cone),
                                    np.sin(1.5*np.pi-tilt-cone)])
x_bldtp = x_bldln + l_pbnd*np.array([np.cos(np.pi-tilt-cone),
                                    np.sin(np.pi-tilt-cone)])

# initialize figure
fig = plt.figure(1,figsize=(7,7))
plt.clf()
ax = plt.axes()

# plot lines
ax.plot([-30,30],[0,0],'k')
ax.plot([0,0],[0,l_tower],':k')
ax.plot([-r_tbase,-r_ttop,r_ttop,r_tbase],
        [0,l_tower,l_tower,0],'k')
ax.plot([x_ttop[0],x_shft[0]],
        [x_ttop[1],x_shft[1]],'k')
ax.plot([x_shft[0],x_hbctr[0]],
        [x_shft[1],x_hbctr[1]],'k')
ax.plot([x_hbctr[0],x_bldrt[0]],
        [x_hbctr[1],x_bldrt[1]],'k')
ax.plot([x_bldrt[0],x_bldln[0]],
        [x_bldrt[1],x_bldln[1]],'k')
ax.plot([x_bldln[0],x_bldtp[0]],
        [x_bldln[1],x_bldtp[1]],'k')

# overlay points of interest
x_tip_undf = (-21.686,31.00)
x_twr_undf = (-3.775,31.00)
undf_clrn  = x_twr_undf[0] - x_tip_undf[0]
ax.plot(0,0,
         'o',mfc='0.8', mec='0.3', ms=9)
ax.plot(x_tip_undf[0],x_tip_undf[1],
         'o',mfc='r', mec='0.3', ms=9)
ax.plot(x_twr_undf[0],x_twr_undf[1],
         'o',mfc='g', mec='0.3', ms=9)
ax.annotate('(0.0, 0.0)',
            xy=(0,0), xycoords='data',
            xytext=(0, -5), textcoords='offset points',
            horizontalalignment='center', verticalalignment='top')
ax.annotate('({:.2f},\n   {:.2f})'.format(*x_tip_undf),
            xy=x_tip_undf, xycoords='data',
            xytext=(-40, 0), textcoords='offset points',
            horizontalalignment='left', verticalalignment='top')
ax.annotate('({:.2f},  \n {:.2f})'.format(*x_twr_undf),
            xy=x_twr_undf, xycoords='data',
            xytext=(-3, 0), textcoords='offset points',
            horizontalalignment='right', verticalalignment='top')

plt.axis('equal')
plt.tight_layout()

if save_figs:
    fig_name = 'DTU10MW_geometry.png'
    fig_path = os.path.join(fig_dir,fig_name)
    fig.savefig(fig_path)


# ================ FIGURES 2+ ================
# simulation results

dll_texts = [['TLBL - htc from source code','DLL : 5 inpvec :  1'],
            ['TLBL - htc from Q0006','DLL : 6 inpvec :  1'],
            ['RINK','DLL : 7 inpvec :  1']]

def ReadBinary(FileName, NrCh, NrSc, ScaleFactor):
    ChVec = range(0, NrCh)
    with open(FileName + '.dat', 'rb') as fid:
        data = np.zeros((NrSc, len(ChVec))); j = 0
        for i in ChVec:
            fid.seek(i * NrSc * 2, 0)
            data[:, j] = np.fromfile(fid, 'int16', NrSc) * ScaleFactor[i]
            j += 1
    return data

file_list = [f.rstrip('.sel') for f in os.listdir(res_dir) if f.endswith('.sel')]

# loop through files
for i_fig, file_name in enumerate(file_list):
    
    # specify number of blades based on filename
    numblades = int(file_name[0])
    
    # load results using wetb
    file_path = os.path.join(res_dir,file_name)
    test = ReadHawc2(file_path)
    data = ReadBinary(file_path, test.NrCh, test.NrSc, test.ScaleFactor)
    
    # pull out and plot DLL results
    fig = plt.figure(2*(i_fig+1),figsize=(6,4.5))
    plt.clf()
    time = data[:,test.ChInfo[2].index('Time')]
    plt.plot(time,undf_clrn*np.ones(time.size),':',
             c='0.3',label='Theoretical undeflected clearance ({:.2f} m)'.format(-x_tip_undf[0] + x_twr_undf[0]))
    for i_dll, dll_text in enumerate(dll_texts):
        dll_desc = [s for s in test.ChInfo[2] if dll_text[1] in s][0]
        ch_idx = test.ChInfo[2].index(dll_desc)
        plt.plot(time,data[:,ch_idx],
                 label=dll_text[0])
    plt.xlim([100,120])
    plt.legend(bbox_to_anchor=(0., 1.03, 1., .102), loc=3,
               ncol=2, mode="expand", borderaxespad=0., fontsize='small')
    plt.tight_layout(rect=[0,0,1,0.90])
    plt.grid('on')
    
    if save_figs:
        fig_name = 'DLL_comparison_{}.png'.format(file_name)
        fig_path = os.path.join(fig_dir,fig_name)
        fig.savefig(fig_path)
      
    # pull out and plot blade tip data
    fig = plt.figure(2*(i_fig+1)+1,figsize=(8,3.5))
    plt.clf()
    plt.plot(x_twr_undf[0]*np.cos(np.linspace(0,2*np.pi,200)),
             x_twr_undf[0]*np.sin(np.linspace(0,2*np.pi,200)),
             'k', label='Tower at z = 31.0 m')
    plt.annotate('Tower surface at z = 31.0 m',
                 xy=(0,-x_twr_undf[0]), xycoords='data',
                 xytext=(5, 5), textcoords='offset points',
                 horizontalalignment='left', verticalalignment='bottom')
    plt.plot(0,x_tip_undf[0],
             'o',mfc='r', mec='0.3', ms=9)
    plt.annotate('undeflected blade tip position\n({:.2f} m from tower edge)'.format(-x_tip_undf[0] + x_twr_undf[0]),
                 xy=(0,x_tip_undf[0]), xycoords='data',
                 xytext=(5, -7), textcoords='offset points',
                 horizontalalignment='left', verticalalignment='top')
    for i_bld in range(numblades):
        xbld_desc = [s for s in test.ChInfo[2] if ('State pos x') in s and \
                     ('gl blade {:.0f} tip pos'.format(i_bld+1)) in s][0]
        ybld_desc = [s for s in test.ChInfo[2] if ('State pos y') in s and \
                     ('gl blade {:.0f} tip pos'.format(i_bld+1)) in s][0]
        xbld_idx = test.ChInfo[2].index(xbld_desc)
        ybld_idx = test.ChInfo[2].index(ybld_desc)
        plt.plot(data[:,xbld_idx],data[:,ybld_idx],
                 label='Blade {:.0f} tip deflections'.format(i_bld+1))
    plt.axis('equal')
    plt.legend(bbox_to_anchor=(0., 1.05, 1., .102), loc=3,
               ncol=2, mode="expand", borderaxespad=0.)
    plt.tight_layout(rect=[0,0,1,0.85])
    plt.grid('on')
    
    if save_figs:
        fig_name = 'topdown_view_{}.png'.format(file_name)
        fig_path = os.path.join(fig_dir,fig_name)
        fig.savefig(fig_path)
        