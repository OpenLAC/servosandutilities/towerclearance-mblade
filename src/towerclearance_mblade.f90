module towerclearance_mblade
   ! =====================================================================================
   ! Author: Jennifer Rinker (rink@dtu.dk)
   !
   ! Compute the clearance between the blade tip and the tower. The DLL takes
   ! a list of points from the htc file and checks the clearance between those
   ! points and the tower surface. The tower is assumed to be a linearly
   ! tapered tube with just shear from the bottom point to the top. The DLL is
   ! suitable for turbines with any number of blades.
   !
   ! The verificaiton of this DLL can be found in the PDF in the GitLab
   ! repository.
   !
   ! The DLL can be compiled on Windows using gfortan (available with mingw or gcc):
   !    $ gfortran -shared -static -o towerclearance_mblade.dll towerclearance_mblade.f90
   !
   ! Example usage of the DLL in HAWC2 to test the blade tip clearance for a
   ! 2-bladed turbine with 11 tower nodes and 50 blade nodes:
   !
   !  begin type2_dll;
   !    name towerclearance_mblade ;
   !    filename  ./control/towerclearance_mblade.dll ;
   !    dll_subroutine_init initialize ;
   !    dll_subroutine_update update ;
   !    arraysizes_init  1 3 ;
   !    arraysizes_update  12 6 ;
   !    begin init ;    Variables passed into initialization function
   !      constant  1 4.15  ; Tower radius at tower bottom [m] 
   !      constant  2 2.75  ; Tower radius at tower top [m]
   !      constant  3    2  ; Number of points to check [-]
   !    end init ;
   !    begin output;   Variables passed into update function
   !      mbdy state pos tower    1 0.0 global ; [1,2,3] global coordinates of tower base
   !      mbdy state pos tower   10 1.0 global ; [4,5,6] global coordinates of tower top
   !      mbdy state pos blade1  49 1.0 global  ; [7,8,9] global coordinates of point 1 (blade 1 tip)
   !      mbdy state pos blade2  49 1.0 global  ; [10,11,12] global coordinates of point 2 (blade 1 tip)
   !    end output;           
   !  end type2_dll;
   !
   !  begin output;
   !   ; Outputs from tower clearence DLL
   !      dll inpvec 5 1 # Min clearance [m];
   !      dll inpvec 5 2 # Idx closest pt to towr [-];
   !      dll inpvec 5 3 # Rel dist tower bottom to top [-];
   !      dll inpvec 5 4 # Tower rad at pt height [m];
   !      dll inpvec 5 5 # Tower center x at pt height [m];
   !      dll inpvec 5 6 # Tower center y at pt height [m];
   !  end output;
   ! =====================================================================================
   implicit none                            ! declare everything explicitly
   !
   ! Initialize module constants
   integer, parameter :: mk = kind(1.0d0)   ! double precision
   real(mk)           :: baseradius         ! radius of tower at base [m]
   integer            :: numpoints          ! number of points to check clearance [-]
   real(mk)           :: topradius          ! radius of tower at top [m]
   !
   contains
   !----------------------------------------------------------------------------------------------!
   ! Initialization subroutine
   !----------------------------------------------------------------------------------------------!
   subroutine initialize(inputs, outputs) bind(C,name="initialize")
      ! Load values defined in htc file into fortran variables
      !
      ! Input array:
      !     1 = base tower radius
      !     2 = top tower radius
      !     3 = number of points to check clearance
      !
      ! Define compiling/linking options
      use ISO_C_BINDING
      !DEC$ ATTRIBUTES DLLEXPORT :: initialize
      implicit none
      ! 
      ! Initialize input and output arrays
      real(mk), dimension(3) :: inputs      ! array passed into the subroutine
      real(mk), dimension(1) :: outputs     ! array returned by the subroutine
      !
      ! Print a status update to the terminal
      print*, 'Tower clearance DLL (mblade, ver. 1.0) loaded...'
      !
      ! Assign values in input array to module constants
      baseradius = inputs(1)        ! tower radius at base
      topradius  = inputs(2)        ! tower radius at top
      numpoints  = inputs(3)        ! number of points to check clearance
   end subroutine initialize
   !
   !----------------------------------------------------------------------------------------------!
   ! Update subroutine
   !----------------------------------------------------------------------------------------------!
   subroutine update(inputs, outputs) bind(C,name="update")
      ! Perform calculations on provided values from HAWC2 and return DLL outputs
      !
      ! Input array:
      !     1:3 = global coordinates of tower base
      !     4:6 = global coordinates of tower top
      !     7:X = global coordinates of points to check clearance
      !
      ! Output array:
      !       1 = minimum clearance
      !       2 = index of point with minimum clearance
      !       3 = relative distance along tower of closest point
      !       4 = radius of tower at height of closest point
      !       5 = x location of tower center at height of closest point
      !       6 = y location of tower center at height of closest point
      !
      ! Define compiling/linking options
      use ISO_C_BINDING
      !DEC$ ATTRIBUTES DLLEXPORT :: update
      implicit none
      ! 
      ! Initialize input and output arrays
      real(mk), dimension(6+3*numpoints) :: inputs              ! array passed into the subroutine
      real(mk), dimension(6)             :: outputs             ! array returned by the subroutine
      !
      ! Initialize subroutine constants/variables
      real(mk)                           :: minclrnce           ! clearance between blade tip and tower surface [m]
      integer                            :: ipoint              ! index counter [-]
      integer                            :: minptidx            ! index of point with minimum clearance [-]
      real(mk)                           :: reldisttower        ! relative distance along tower where closest point is [-]
      real(mk)                           :: testclrnc           ! clearance between test point and tower surface [m]
      real(mk), dimension(3)             :: testpoint           ! global coordinates of point to check clearance [m]
      real(mk)                           :: testreltwdist       ! relative distance along tower of test point [-]
      real(mk)                           :: testtwrrad          ! tower radius at height of test point [m]
      real(mk)                           :: testtwrx            ! tower x coordinate at height of test point [m]
      real(mk)                           :: testtwry            ! tower y coordinate at height of test point [m]
      real(mk), dimension(3)             :: towerbase           ! global coordinates of tower bottom [m]
      real(mk)                           :: towerradz           ! tower radius at height of blade tip [m]
      real(mk), dimension(3)             :: towertop            ! global coordinates of tower top [m]
      real(mk)                           :: towerxz             ! tower x coordinate at height of blade tip [m]
      real(mk)                           :: toweryz             ! tower y coordinate at height of blade tip [m]
      !
      ! Pull out tower base and top positions from the input array
      towerbase(1:3) = inputs(1:3)
      towertop(1:3)  = inputs(4:6)
      !
      ! Loop through test points to analyse
      minclrnce = 2*abs(towertop(3))     ! initialize minimum clearance to 2*towerheight
      do ipoint = 1, numpoints
         !
         ! Pull data from input array
         testpoint(1:3) = inputs(3*(ipoint-1)+7:3*ipoint+6)
         !
         ! If the point is above the tower top, set coordinates to dummy values for large clearance
         if (testpoint(3) .LT. towertop(3)) then
            testpoint(1) = abs(towertop(3))     ! set x coordinate to tower height
            testpoint(2) = abs(towertop(3))     ! set y coordinate to tower height
            testpoint(3) = abs(towertop(3))     ! set z coordinate to tower height
         end if
         !
         ! Linearly interpolate the center of the tower and tower radius
         !    according to the vertical height of the test point
         testreltwdist  = (testpoint(3)-towerbase(3))/(towertop(3)-towerbase(3))
         testtwrrad     = (topradius - baseradius)*testreltwdist + baseradius
         testtwrx       = (towertop(1) - towerbase(1))*testreltwdist + towerbase(1)
         testtwry       = (towertop(2) - towerbase(2))*testreltwdist + towerbase(2)
         !
         ! Calculate the distance between the test point and tower center
         testclrnc = sqrt((testpoint(1) - testtwrx)*(testpoint(1) - testtwrx) + &
                           (testpoint(2) - testtwry)*(testpoint(2) - testtwry)) - testtwrrad
         !
         ! if this new clearance is smaller than the old clearance,
         !    overwrite values returned by DLL
         if (testclrnc .LT. minclrnce) then
            minclrnce    = testclrnc
            minptidx     = ipoint
            reldisttower = testreltwdist
            towerradz    = testtwrrad
            towerxz      = testtwrx
            toweryz      = testtwry
         end if
      enddo
      !
      ! Set as output the minimum among the three blades...
      outputs(1) = minclrnce        ! minimum clearance [m]
      outputs(2) = minptidx         ! index of point with minimum clearance [-]
      outputs(3) = reldisttower     ! relative distance along tower of closest point [-]
      outputs(4) = towerradz        ! radius of tower at height of closest point [m]
      outputs(5) = towerxz          ! x location of tower center at height of closest point [m]
      outputs(6) = toweryz          ! y location of tower center at height of closest point [m]
   end subroutine update
   !----------------------------------------------------------------------------------------------!
end module towerclearance_mblade
