# Overview

[![pipeline status](https://gitlab.windenergy.dtu.dk/OpenLAC/servosandutilities/towerclearance-mblade/badges/master/pipeline.svg)](https://gitlab.windenergy.dtu.dk/OpenLAC/servosandutilities/towerclearance-mblade/-/commits/master)

This DLL is designed for tower clearance calculations in HAWC2 models. The DLL
calculates the clearance between specified points on the blade and the 
surface of the tower. The usage is similar to Carlo Tibaldi's 
towerclearance.dll, but the methodology is substantially different. The DLL 
has been verified against theoretical calculations (see section below).

## Repository contents

- ```./README.md```: contains all of the background information regarding how 
the code works  
- ```./src/```: Fortran source code and linux makefile
- ```./verification/```: code and results to verify DLL
- ```./print_dll_inputs.py```: script to write the DLL inputs for one or more
htc files

Developers, a summary of the git workflow is at the bottom of the readme.

# Practical Information

## Downloading Compiled DLL

This repo is automatically configured to compile the DLLs using `mingw-w64`.
Both 32-bit and 64-bit DLLs are available; you must use the version that matches
your HAWC2 executable.

The compiled 32- and 64-bit DLLs for different releases can be downloaded under
the "Repository - Tags" page.

## Usage and Inputs/Outputs

Here is example code of using the DLL in HAWC2 to test the blade tip 
clearance for a 2-bladed turbine with 11 tower nodes and 50 blade nodes. The 
DLL inputs and outputs are defined here and also in the comments of the 
Fortran code.

Note that the type2_dll code block can be automatically generated for a set of
htc files by updating the script inputs in ```print_dll_inputs.py``` (included
in this repository) and then running the script. The code block will then be
written to a text file and can be copied into the htc file.

```
begin type2_dll;
  name towerclearance_mblade ;
  filename  ./control/towerclearance_mblade.dll ;
  dll_subroutine_init initialize ;
  dll_subroutine_update update ;
  arraysizes_init  1 3 ;
  arraysizes_update  12 6 ;
  begin init ;    Variables passed into initialization function
    constant  1 4.15  ; Tower radius at tower bottom [m] 
    constant  2 2.75  ; Tower radius at tower top [m]
    constant  3    2  ; Number of points to check [-]
  end init ;
  begin output;   Variables passed into update function
    mbdy state pos tower    1 0.0 global ; [1,2,3] global coordinates of tower base
    mbdy state pos tower   10 1.0 global ; [4,5,6] global coordinates of tower top
    mbdy state pos blade1  49 1.0 global  ; [7,8,9] global coordinates of point 1 (blade 1 tip)
    mbdy state pos blade2  49 1.0 global  ; [10,11,12] global coordinates of point 2 (blade 1 tip)
  end output;           
end type2_dll;

begin output;
 ; Outputs from tower clearence DLL
    dll inpvec 5 1 # Min clearance [m];
    dll inpvec 5 2 # Idx closest pt to towr [-];
    dll inpvec 5 3 # Rel dist tower bottom to top [-];
    dll inpvec 5 4 # Tower rad at pt height [m];
    dll inpvec 5 5 # Tower center x at pt height [m];
    dll inpvec 5 6 # Tower center y at pt height [m];
end output;
```

## Compiling on Unix (as a shared object)

To compile for Unix, go to the /src subfolder and type:

```>> make FC=compiler```

Where 'compiler' should be replaced by a suitable compiler already installed in the system (e.g. ifort, mpif90).

# Background

## History

The original plan was not to rewrite Tibaldi's DLL, but just to modify it so
that it could be used with turbines that do not have 3 blades. However, the DLL
was not producing correct results with the 2-bladed turbine. Specifically, the
minumum tower clearance for a low-wind case with the 2-bladed DTU 10 MW was
approximately 24 m. However, the theoretical undeflected tower clearance is
approximately 18 m. So, the clearance calculations were off by about 6 m.

Extensive debugging only produced more issues, so in the end the DLL was
rewritten from scratch. Hopefully this new version is correct and easy to
comprehend.

## Methodology

This DLL uses a fairly simple methodology. A series of test points (e.g., 
blade tips) are passed in by the user. The clearance for each test point is 
calculated by determining the distance between the point and the interpolated 
tower surface in a horizontal plane. Any test point with a height above the 
tower top is given a clearance equal to the tower height. The tower surface 
is assumed to be a circle with a center position and radius linearly 
interpolated from the centers and radii of the top and bottom sections of the 
tower. The returned minimum clearance is the minimum of all test point 
clearances.

## Verification

This DLL was verified by comparing the outputs from 2-bladed and 3-bladed 
versions of the DTU 10 MW with no aerodynamics to theoretical calculations of 
the clearance. A document with the calculation derivations and plots is in 
the ```verification/``` folder.  

# Git workflow

Uses the branch strategy discussed in detail [by Stan Sarr](https://medium.com/@stansarr/git-workflow-branches-strategy-4d29f9b2a417).
Branches:  
 * **Feature**. Off of develop.
 * **Develop**. Off of master. Protected, merge only.
 * **Release**. Off of develop. Protected, merge only. 
   Merges are only documentation and bug fixes.
   Merged into both master and into develop.
 * **Master**. Protected, only merges from release branch or hotfix.
   Tagged with version.
 * **Hotfix**. Only show-stopping bugs in master.

# Contact

Please Jenni Rinker at rink@dtu.dk if you have questions or issues with this DLL.
